package de.foodsharing.ui.basket

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.whenever
import de.foodsharing.api.BasketAPI
import de.foodsharing.model.User
import de.foodsharing.services.AuthService
import de.foodsharing.services.BasketService
import de.foodsharing.services.ProfileService
import de.foodsharing.test.configureTestSchedulers
import de.foodsharing.test.createRandomBasket
import de.foodsharing.test.createRandomProfile
import de.foodsharing.utils.CurrentUserLocation
import de.foodsharing.utils.UserLocation
import io.reactivex.Observable.just
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class BasketViewModelTest {

    @Mock
    lateinit var basketService: BasketService

    @Mock
    lateinit var authService: AuthService

    @Mock
    lateinit var profileService: ProfileService

    @Mock
    lateinit var currentUserLocation: CurrentUserLocation

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        configureTestSchedulers()
    }

    @Test
    fun `fetch basket`() {
        val profile = createRandomProfile(hasAddress = true)
        val user = User(profile.id, profile.firstname, null, 0)
        whenever(authService.currentUser()) doReturn just(user)
        whenever(profileService.current()) doReturn just(profile)
        whenever(currentUserLocation.currentUserCoordinates) doReturn (MutableLiveData())
        val userLocation = UserLocation(profileService, currentUserLocation)

        val basket = createRandomBasket()
        whenever(basketService.get(anyInt())) doReturn just(
            BasketAPI.BasketResponse().apply {
                this.basket = basket
            }
        )

        val viewModel = BasketViewModel(basketService, authService, userLocation)
        viewModel.basketId = basket.id

        val b = viewModel.basket.value!!
        Assert.assertEquals(b, basket)
    }
}
