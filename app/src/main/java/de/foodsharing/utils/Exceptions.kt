package de.foodsharing.utils

import android.util.Log
import io.sentry.Sentry

fun captureException(t: Throwable) {
    Log.e(LOG_TAG, t.localizedMessage, t)
    Sentry.captureException(t)
}

fun captureException(message: String) {
    val e = IllegalStateException(message)
    e.fillInStackTrace()
    Sentry.captureException(e)
}

class MissingProfileException(
    val profileId: Int
) : Exception("Profile with id $profileId is missing.")
