package de.foodsharing.utils

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import io.reactivex.Observable
import okhttp3.OkHttpClient
import okhttp3.Request
import java.io.InputStream
import javax.inject.Inject

/**
 * Loads network resources (images, ...) via OkHttp, thereby using it's caching functionality.
 */
open class CachedResourceLoader @Inject constructor(private val client: OkHttpClient) {

    private fun load(url: String?): InputStream? = url?.let {
        val request = Request.Builder().url(url).build()
        val response = client.newCall(request).execute()
        return response.body?.byteStream()
    }

    /**
     * The observable is responsible for loading a network resource.
     */
    fun createObservable(url: String?): Observable<InputStream> =
        Observable.just(url).map { load(url) }

    /**
     * The observable is responsible for loading one image and optionally falling back to a default
     * image.
     */
    fun createImageObservable(url: String?, defaultImage: String? = null): Observable<Bitmap> =
        createObservable(url)
            .map { BitmapFactory.decodeStream(it) }
            .onErrorReturn {
                BitmapFactory.decodeStream(load(defaultImage))
            }
}
