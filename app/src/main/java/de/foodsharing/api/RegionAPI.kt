package de.foodsharing.api

import de.foodsharing.model.Community
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path

interface RegionAPI {

    @GET("/api/map/regions/{id}")
    fun communityDescription(@Path("id") regionId: Int): Observable<Community>
}
