package de.foodsharing.model

import java.util.Date

/**
 * A chat message that is part of a [Conversation].
 */
data class Message(
    val id: Int,
    val body: String,
    val sentAt: Date,
    val author: User
)
