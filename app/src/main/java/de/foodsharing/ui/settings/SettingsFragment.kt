package de.foodsharing.ui.settings

import android.os.Bundle
import androidx.preference.Preference
import androidx.preference.PreferenceCategory
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.get
import de.foodsharing.BuildConfig
import de.foodsharing.R

class SettingsFragment : PreferenceFragmentCompat() {
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.root_preferences, rootKey)

        if (BuildConfig.FLAVOR == "play") {
            // Show notification settings
            preferenceScreen.get<PreferenceCategory>(
                requireContext().getString(R.string.preferenceCategoryPushNotifications)
            )?.isVisible = true
        }

        // restart the activity if the language was changed
        preferenceScreen.get<Preference>(requireContext().getString(R.string.preferenceLanguage))
            ?.setOnPreferenceChangeListener { preference: Preference, newValue: Any ->
                activity?.recreate()
                true
            }
    }
}
