package de.foodsharing.ui.initial

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import de.foodsharing.R
import de.foodsharing.databinding.ErrorStateBinding
import de.foodsharing.di.Injectable
import de.foodsharing.services.AuthService
import de.foodsharing.ui.base.BaseActivity
import de.foodsharing.ui.login.LoginActivity
import de.foodsharing.ui.main.MainActivity
import de.foodsharing.utils.ConnectivityReceiver
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class InitialActivity : BaseActivity(), Injectable {

    private lateinit var binding: ErrorStateBinding

    @Inject
    lateinit var auth: AuthService

    private val subscriptions = CompositeDisposable()

    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ErrorStateBinding.inflate(layoutInflater)
        setContentView(binding.root)

        subscriptions.add(
            auth.check()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .retryWhen { obs ->
                    obs.switchMap { error ->
                        error.printStackTrace()
                        Observable.create<Unit> { e ->
                            binding.errorView.visibility = View.VISIBLE

                            if (!this.isConnected) {
                                binding.errorTextView.text = getText(R.string.error_no_connection)

                                ConnectivityReceiver.observe().filter { it }.firstElement().subscribe {
                                    if (!e.isDisposed && it) {
                                        e.onNext(Unit)
                                        binding.errorView.visibility = View.GONE
                                        binding.retryButton.setOnClickListener(null)
                                    }
                                }
                            } else {
                                binding.errorTextView.text = getText(R.string.error_unknown)
                            }

                            binding.retryButton.setOnClickListener {
                                e.onNext(Unit)
                                binding.errorView.visibility = View.GONE
                                binding.retryButton.setOnClickListener(null)
                            }
                            e.setCancellable {
                                binding.errorView.visibility = View.GONE
                                binding.retryButton.setOnClickListener(null)
                            }
                        }
                    }
                }
                .subscribe { isLoggedIn ->
                    preferences.isLoggedIn = isLoggedIn
                    if (isLoggedIn) MainActivity.start(this) else LoginActivity.start(this)
                    finish()
                }
        )
    }

    override fun onDestroy() {
        subscriptions.dispose()
        super.onDestroy()
    }
}
