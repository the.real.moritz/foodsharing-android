package de.foodsharing.ui.main

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.app.ShareCompat
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.tabs.TabLayout
import de.foodsharing.BuildConfig
import de.foodsharing.R
import de.foodsharing.databinding.ActivityMainBinding
import de.foodsharing.di.Injectable
import de.foodsharing.ui.base.BaseActivity
import de.foodsharing.ui.base.EventObserver
import de.foodsharing.ui.login.LoginActivity
import de.foodsharing.ui.map.MapActivity
import de.foodsharing.ui.profile.ProfileActivity
import de.foodsharing.ui.settings.SettingsActivity
import de.foodsharing.utils.LINK_BASE_URL
import de.foodsharing.utils.Utils
import javax.inject.Inject

class MainActivity : BaseActivity(), Injectable {

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, MainActivity::class.java)
            context.startActivity(intent)
        }

        const val PERMISSIONS_POST_RC = 43352
    }

    private lateinit var binding: ActivityMainBinding

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val mainViewModel: MainViewModel by viewModels { viewModelFactory }

    private val pagerAdapter = MainPagerAdapter(supportFragmentManager)
    private lateinit var drawerToggle: ActionBarDrawerToggle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowTitleEnabled(false)
        }

        drawerToggle = ActionBarDrawerToggle(
            this,
            binding.drawerLayout,
            binding.toolbar,
            R.string.drawer_open,
            R.string.drawer_close
        )
        drawerToggle.syncState()

        setupTabs()

        // set actions for menu items in the drawer
        binding.navView.setNavigationItemSelectedListener { menuItem ->
            // close drawer when item is tapped
            binding.drawerLayout.closeDrawers()

            when (menuItem.itemId) {
                R.id.nav_profile -> showProfile()
                R.id.nav_logout -> logout()
                R.id.nav_support -> Utils.openSupportEmail(this, R.string.support_email_subject_suffix)
                R.id.nav_shareApp -> shareApp()
                R.id.nav_Website -> openWebsite()
                R.id.nav_settings -> openSettings()
                R.id.nav_map -> {
                    MapActivity.start(this)
                    overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
                }
            }

            true
        }

        binding.versionTextView.text = getString(
            R.string.version,
            BuildConfig.VERSION_NAME,
            BuildConfig.VERSION_CODE,
            BuildConfig.FLAVOR
        )

        bindViewModel()

        if (preferences.isSentryEnabled == null) {
            Utils.showQuestionDialog(this, getString(R.string.use_sentry_question), true) { result ->
                preferences.isSentryEnabled = result
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSIONS_POST_RC) {
            if (grantResults.first() == PackageManager.PERMISSION_DENIED) {
                preferences.pushNotificationsEnabled = null
            } else {
                preferences.pushNotificationsEnabled = true
            }
        }
    }

    private fun showProfile() {
        ProfileActivity.start(this, mainViewModel.currentUserId.value!!)
    }

    private fun openWebsite() {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(LINK_BASE_URL))
        startActivity(browserIntent)
    }

    private fun shareApp() {
        val shareIntent = ShareCompat.IntentBuilder(this)
            .setType("text/plain")
            .setText(getString(R.string.share_recommend_text))
            .intent
        startActivity(Intent.createChooser(shareIntent, getString(R.string.share_dialog_title)))
    }

    private fun setupTabs() {
        binding.mainPager.adapter = pagerAdapter
        binding.mainTabLayout.setupWithViewPager(binding.mainPager)

        val context = this
        val initiallySelectedTabIndex = 0
        val tabColor = R.color.tabTextForeground
        val tabColorSelected = R.color.secondary500

        binding.mainTabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {

            override fun onTabSelected(tab: TabLayout.Tab?) {
                val textView = tab?.customView as TextView
                textView.setTextColor(ContextCompat.getColor(context, tabColorSelected))
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                val textView = tab?.customView as TextView
                textView.setTextColor(ContextCompat.getColor(context, tabColor))
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {}
        })

        for (position in 0 until pagerAdapter.count) {
            binding.mainTabLayout.getTabAt(position)?.apply {
                val textView = LayoutInflater.from(context)
                    .inflate(pagerAdapter.getPageTextView(position), null) as TextView
                if (position == initiallySelectedTabIndex) {
                    textView.setTextColor(ContextCompat.getColor(context, tabColorSelected))
                } else {
                    textView.setTextColor(ContextCompat.getColor(context, tabColor))
                }
                customView = textView
            }
        }
    }

    private fun bindViewModel() {
        mainViewModel.currentUserName.observe(this) {
            // Show the current user name in the side bar
            binding.navView.getHeaderView(0).findViewById<TextView>(R.id.account_name)?.text = it
        }

        mainViewModel.popup.observe(
            this,
            EventObserver {
                Utils.handlePopup(this, it)
            }
        )
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        drawerToggle.onConfigurationChanged(newConfig)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                binding.drawerLayout.openDrawer(GravityCompat.START)
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    /**
     * Logs out the user and switches to the LoginActivity. This is called when the logout button
     * in the drawer is selected.
     */
    private fun logout() {
        mainViewModel.logout()
        LoginActivity.start(this)
        finish()
    }

    private fun openSettings() {
        SettingsActivity.start(this)
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
    }
}
