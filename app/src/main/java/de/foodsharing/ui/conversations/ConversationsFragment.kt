package de.foodsharing.ui.conversations

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.transition.ChangeBounds
import androidx.transition.Slide
import androidx.transition.TransitionManager
import androidx.transition.TransitionSet
import com.google.android.material.snackbar.Snackbar
import com.stfalcon.chatkit.dialogs.DialogsListAdapter
import de.foodsharing.BuildConfig
import de.foodsharing.R
import de.foodsharing.databinding.FragmentConversationListBinding
import de.foodsharing.di.Injectable
import de.foodsharing.ui.base.BaseFragment
import de.foodsharing.ui.conversation.ChatkitMessage
import de.foodsharing.ui.conversation.ConversationActivity
import de.foodsharing.ui.main.MainActivity
import de.foodsharing.utils.ChatkitGlideImageLoader
import de.foodsharing.utils.withColor
import javax.inject.Inject

class ConversationsFragment :
    BaseFragment(),
    DialogsListAdapter.OnDialogClickListener<ChatkitConversation>,
    Injectable {

    private var _binding: FragmentConversationListBinding? = null

    private val binding get() = _binding!!

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: ConversationsViewModel by viewModels { viewModelFactory }

    private lateinit var adapter: FsDialogListAdapter
    private var errorSnackbar: Snackbar? = null

    // Number of items at the end of the list before starting to load the next page
    private val NUM_PREFETCH_ITEMS = 10

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentConversationListBinding.inflate(inflater, container, false)

        val layoutManager = LinearLayoutManager(activity)
        binding.recyclerView.layoutManager = layoutManager
        adapter = FsDialogListAdapter(ChatkitGlideImageLoader(requireContext()))
        adapter.setOnDialogClickListener(this)
        adapter.setDatesFormatter(ConversationDateFormatter(requireContext()))
        binding.recyclerView.setAdapter(adapter)

        binding.pullRefresh.setOnRefreshListener {
            viewModel.refresh()
        }

        // listener that notices if the user scrolled to the bottom of the conversations list
        binding.recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                val linLayoutManager = binding.recyclerView.layoutManager as? LinearLayoutManager
                val remainingItems = adapter.itemCount - (linLayoutManager?.findLastVisibleItemPosition() ?: -1)
                if (remainingItems < NUM_PREFETCH_ITEMS) {
                    viewModel.loadNext()
                }
            }
        })

        binding.noConversationsLabel.movementMethod = LinkMovementMethod.getInstance()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        bindViewModel()
        handlePushNotificationSnackbar()
    }

    class BannerTransition : TransitionSet() {
        init {
            init()
        }

        private fun init() {
            ordering = ORDERING_TOGETHER

            addTransition(ChangeBounds())
                .addTransition(Slide(Gravity.TOP))
        }
    }

    private fun handlePushNotificationSnackbar() {
        if (BuildConfig.FLAVOR == "play" && preferences.pushNotificationsEnabled == null) {
            binding.banner.visibility = View.VISIBLE
            binding.buttonPushLater.setOnClickListener {
                TransitionManager.beginDelayedTransition(binding.innerContainer, BannerTransition())
                binding.banner.visibility = View.GONE
                preferences.pushNotificationsEnabled = false
            }

            binding.buttonPushActivate.setOnClickListener {
                TransitionManager.beginDelayedTransition(binding.innerContainer, BannerTransition())
                binding.banner.visibility = View.GONE
                preferences.pushNotificationsEnabled = true
                checkPostPermission()
            }
        } else {
            binding.banner.visibility = View.GONE
        }
    }

    private fun checkPostPermission() {
        if (Build.VERSION.SDK_INT < 33) {
            return
        }

        val permissionCheckResult =
            ContextCompat.checkSelfPermission(requireActivity(), android.Manifest.permission.POST_NOTIFICATIONS)
        if (permissionCheckResult == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(android.Manifest.permission.POST_NOTIFICATIONS),
                MainActivity.PERMISSIONS_POST_RC
            )
        }
    }

    private fun bindViewModel() {
        val diff = AsyncListDiffer(
            adapter,
            object : DiffUtil.ItemCallback<ChatkitConversation>() {
                override fun areItemsTheSame(oldItem: ChatkitConversation, newItem: ChatkitConversation): Boolean {
                    return oldItem.id == newItem.id
                }

                override fun areContentsTheSame(
                    oldItem: ChatkitConversation,
                    newItem: ChatkitConversation
                ): Boolean {
                    return oldItem == newItem
                }
            }
        )

        adapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                val linearLayoutManager = binding.recyclerView.layoutManager as? LinearLayoutManager
                val firstPosition = linearLayoutManager?.findFirstVisibleItemPosition()
                if (positionStart == 0 && firstPosition == 0) {
                    linearLayoutManager.scrollToPositionWithOffset(0, 0)
                }
            }

            override fun onItemRangeMoved(fromPosition: Int, toPosition: Int, itemCount: Int) {
                val linearLayoutManager = binding.recyclerView.layoutManager as? LinearLayoutManager
                val firstPosition = linearLayoutManager?.findFirstVisibleItemPosition()
                if ((toPosition == 0 || fromPosition == 0) && firstPosition == 0) {
                    linearLayoutManager.scrollToPositionWithOffset(0, 0)
                }
            }
        })

        viewModel.conversationsWithCurrentUser.observe(
            viewLifecycleOwner,
            Observer {
                val conversations = it.first ?: return@Observer
                val currentUser = it.second ?: return@Observer

                val updatedList = conversations.map { c ->
                    ChatkitConversation(
                        c,
                        c.lastMessage?.let { m ->
                            ChatkitMessage(m, m.author)
                        },
                        currentUser.id
                    )
                }

                diff.submitList(updatedList) {
                    adapter.setItemsWithoutNotify(diff.currentList)
                }

                binding.noConversationsLabel.visibility = if (conversations.isEmpty()) {
                    View.VISIBLE
                } else {
                    View.GONE
                }

                binding.recyclerView.visibility = if (conversations.isEmpty()) {
                    View.GONE
                } else {
                    View.VISIBLE
                }
            }
        )

        viewModel.isLoading.observe(viewLifecycleOwner) {
            binding.progressBar.visibility = if (it) View.VISIBLE else View.GONE
        }

        viewModel.isReloading.observe(viewLifecycleOwner) {
            binding.pullRefresh.isRefreshing = it
        }

        viewModel.errorState.observe(viewLifecycleOwner) {
            errorSnackbar?.dismiss()
            val rootView = view
            val fragmentContext = context
            if (it != null && rootView != null && fragmentContext != null) {
                errorSnackbar = Snackbar.make(
                    rootView,
                    it,
                    Snackbar.LENGTH_INDEFINITE
                )
                    .setAction(R.string.retry_button) {
                        viewModel.tryAgain()
                    }.setActionTextColor(ContextCompat.getColor(fragmentContext, R.color.white))
                    .withColor(ContextCompat.getColor(fragmentContext, R.color.colorSecondary))
                errorSnackbar?.show()
            }
        }
    }

    override fun onDialogClick(conversation: ChatkitConversation) {
        // show conversation
        var message: String? = null
        if (activity?.intent?.action == Intent.ACTION_SEND && activity?.intent?.type == "text/plain") {
            message = activity?.intent?.getStringExtra(Intent.EXTRA_TEXT)
        }
        ConversationActivity.start(requireContext(), conversation.conversation.id, message)
        activity?.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        adapter.setOnDialogClickListener(null)
        errorSnackbar?.dismiss()
        errorSnackbar = null
    }
}
