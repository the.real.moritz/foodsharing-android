package de.foodsharing.ui.conversations

import android.os.Bundle
import android.view.MenuItem
import androidx.lifecycle.ViewModelProvider
import dagger.android.support.DaggerAppCompatActivity
import de.foodsharing.R
import de.foodsharing.databinding.ActivityShareTextToConversationBinding
import javax.inject.Inject

class ShareTextToConversationActivity : DaggerAppCompatActivity() {

    private lateinit var binding: ActivityShareTextToConversationBinding

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityShareTextToConversationBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setTitle(R.string.send_message_to)
        }

        supportFragmentManager.beginTransaction().add(R.id.fragment_container, ConversationsFragment()).commit()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }
}
