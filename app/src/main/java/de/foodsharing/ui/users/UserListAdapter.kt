package de.foodsharing.ui.users

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import de.foodsharing.R
import de.foodsharing.databinding.ItemUserBinding
import de.foodsharing.model.User
import de.foodsharing.utils.Utils
import de.foodsharing.utils.getDisplayName

class UserListAdapter(
    private val onClickListener: (User) -> Unit,
    val context: Context
) : RecyclerView.Adapter<UserListAdapter.UserHolder>() {
    private var users = emptyList<User>()

    override fun getItemCount() = users.size

    override fun onBindViewHolder(holder: UserHolder, position: Int) {
        if (position in 0 until itemCount) {
            holder.bind(users[position])
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewInt: Int): UserHolder {
        val binding = ItemUserBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return UserHolder(binding, onClickListener, context)
    }

    fun setUsers(users: List<User>) {
        this.users = users
        notifyDataSetChanged()
    }

    class UserHolder(
        private val binding: ItemUserBinding,
        private val onClickListener: (User) -> Unit,
        private val context: Context
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(user: User) {
            binding.root.setOnClickListener {
                onClickListener(user)
            }

            binding.itemName.text = user.getDisplayName(context)
            Glide.with(context)
                .load(Utils.getUserPhotoURL(user, Utils.PhotoType.Q_130, 130))
                .fitCenter()
                .centerCrop()
                .error(R.drawable.default_user_picture)
                .into(binding.itemPicture)
        }
    }
}
