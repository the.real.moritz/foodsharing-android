package de.foodsharing.ui.basket

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.View.GONE
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import android.widget.EditText
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.core.os.bundleOf
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.google.android.material.snackbar.Snackbar
import de.foodsharing.R
import de.foodsharing.databinding.ActivityBasketBinding
import de.foodsharing.di.Injectable
import de.foodsharing.model.Basket
import de.foodsharing.services.PreferenceManager
import de.foodsharing.ui.base.BaseActivity
import de.foodsharing.ui.base.EventObserver
import de.foodsharing.ui.conversation.ConversationActivity
import de.foodsharing.ui.editbasket.EditBasketActivity
import de.foodsharing.ui.picture.PictureActivity
import de.foodsharing.ui.profile.ProfileActivity
import de.foodsharing.utils.BASKET_MAP_ZOOM
import de.foodsharing.utils.LINK_BASE_URL
import de.foodsharing.utils.OsmdroidUtils
import de.foodsharing.utils.Utils
import de.foodsharing.utils.Utils.getBasketMarkerIconBitmap
import de.foodsharing.utils.getDisplayName
import de.foodsharing.utils.setGone
import de.foodsharing.utils.setVisible
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.Locale
import javax.inject.Inject

class BasketActivity : BaseActivity(), Injectable {

    private lateinit var binding: ActivityBasketBinding

    companion object {
        private const val BASKET_URL = "$LINK_BASE_URL/essenskoerbe/%d"

        private const val EXTRA_BASKET_ID = "id"

        fun start(context: Context, basketId: Int) {
            val extras = bundleOf(EXTRA_BASKET_ID to basketId)
            val intent = Intent(context, BasketActivity::class.java).putExtras(extras)
            context.startActivity(intent)
        }
    }

    private val dateFormat = SimpleDateFormat("EEE, d MMM yyyy\nHH:mm", Locale.getDefault())
    private val dateFormatShort = SimpleDateFormat("EEE, d MMM\nHH:mm", Locale.getDefault())

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var preferenceManager: PreferenceManager

    private val viewModel: BasketViewModel by viewModels { viewModelFactory }
    private var mapSnapshotDetachAction: (() -> Unit)? = null
    private var menu: Menu? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityBasketBinding.inflate(layoutInflater)
        rootLayoutID = binding.root.id
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        bindViewModel()

        if (intent.hasExtra(EXTRA_BASKET_ID)) {
            val id = intent.getIntExtra(EXTRA_BASKET_ID, -1)
            supportActionBar?.title = "${getString(R.string.basket_label_short)} #$id"
            viewModel.basketId = id
        } else if (intent.data?.pathSegments?.firstOrNull() == "essenskoerbe") {
            val id = intent.data!!.pathSegments.last().toIntOrNull()
            if (id != null) {
                supportActionBar?.title = "${getString(R.string.basket_label_short)} #$id"
                viewModel.basketId = id
            } else {
                showBasket404()
            }
        } else {
            showBasket404()
        }
    }

    private fun bindViewModel() {
        viewModel.isLoading.observe(this) {
            binding.progressBar.visibility = if (it) VISIBLE else INVISIBLE
        }

        viewModel.showInfo.observe(
            this,
            EventObserver {
                Toast.makeText(this, getString(it), Toast.LENGTH_LONG).show()
            }
        )

        viewModel.showError.observe(
            this,
            EventObserver {
                showErrorMessage(getString(it))
            }
        )

        viewModel.basket.observe(this) {
            it?.let { it1 -> display(it1) }
        }

        viewModel.basketRemoved.observe(
            this,
            EventObserver {
                finish()
            }
        )

        viewModel.isCurrentUser.observe(this) {
            updateCurrentUser(it)
        }

        viewModel.distance.observe(this) {
            binding.distanceToBasket.text = Utils.formatDistance(it)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.basket_menu, menu)
        this.menu = menu
        updateCurrentUser(viewModel.isCurrentUser.value == true)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }

        R.id.basket_copy_button -> {
            getBasketUrl()?.let {
                Utils.copyToClipboard(this, it)
                Toast.makeText(this, getString(R.string.copied_url), Toast.LENGTH_SHORT).show()
            }
            true
        }

        R.id.basket_share_button -> {
            getBasketUrl()?.let {
                Utils.openShareDialog(this, it)
            }
            true
        }

        R.id.basket_remove_button -> {
            viewModel.basket.value?.let {
                Utils.showQuestionDialog(this, getString(R.string.basket_remove_question)) { result ->
                    if (result) {
                        binding.progressBar.visibility = VISIBLE
                        viewModel.removeBasket()
                    }
                }
            }
            true
        }

        R.id.basket_edit_button -> {
            viewModel.basket.value?.let {
                EditBasketActivity.start(this, it)
            }
            true
        }

        R.id.basket_open_website_button -> {
            getBasketUrl()?.let {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(it))
                startActivity(browserIntent)
            }
            true
        }

        else -> super.onOptionsItemSelected(item)
    }

    private fun getBasketUrl(): String? {
        return viewModel.basket.value?.let { BASKET_URL.format(it.id) }
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right)
    }

    override fun onDestroy() {
        mapSnapshotDetachAction?.invoke()
        super.onDestroy()
    }

    private fun display(basket: Basket) {
        binding.basketCreatedAt.text = formatDate(basket.createdAt)
        binding.basketValidUntil.text = formatDate(basket.until)
        binding.basketDescription.text = basket.description
        binding.basketCreatorName.text = basket.creator.getDisplayName(this)

        invalidateOptionsMenu()

        binding.basketContentView.visibility = VISIBLE
        binding.progressBar.visibility = GONE

        // load basket picture
        if (basket.picture.isNullOrEmpty()) {
            binding.basketPicture.visibility = GONE
        } else {
            basket.picture?.let {
                val photoType =
                    if (preferenceManager.useLowResolutionImages) {
                        Utils.BasketPhotoType.MEDIUM
                    } else {
                        Utils.BasketPhotoType.NORMAL
                    }
                val pictureUrl = Utils.getBasketPhotoURL(it, photoType)
                Glide.with(this)
                    .load(pictureUrl)
                    .centerCrop()
                    .error(R.drawable.basket_default_picture)
                    .into(binding.basketPicture)

                binding.basketPicture.setOnClickListener {
                    PictureActivity.start(this, pictureUrl)
                    overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
                }
                binding.basketPicture.visibility = VISIBLE
            }
        }
        // load user picture

        Glide.with(this)
            .load(Utils.getUserPhotoURL(basket.creator, Utils.PhotoType.Q_130, 130))
            .placeholder(R.drawable.default_user_picture)
            .error(R.drawable.default_user_picture)
            .transform(CircleCrop())
            .into(binding.avatar)

        val listener = View.OnClickListener {
            ProfileActivity.start(this, basket.creator)
            overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
        }
        binding.avatar.setOnClickListener(listener)
        binding.basketCreatorName.setOnClickListener(listener)

        updateCurrentUser(viewModel.isCurrentUser.value == true)

        val basketTitle = getString(R.string.basket_title, basket.creator.getDisplayName(this))

        mapSnapshotDetachAction = OsmdroidUtils.loadMapTileToImageView(
            binding.basketLocationView,
            basket.toCoordinate(),
            BASKET_MAP_ZOOM,
            getBasketMarkerIconBitmap(this.applicationContext),
            preferenceManager.allowHighResolutionMap
        )
        binding.basketLocationView.setOnClickListener {
            Utils.openCoordinate(this, basket.toCoordinate(), basketTitle)
        }
    }

    private fun showErrorMessage(error: String) {
        binding.progressBar.visibility = GONE
        showMessage(error, duration = Snackbar.LENGTH_LONG)
    }

    private fun showBasket404() {
        showErrorMessage(getString(R.string.basket_404))
    }

    /**
     * Updates the visibility of the option menu items and contact labels depending on whether the
     * current user is the owner of the basket.
     */
    private fun updateCurrentUser(isCurrentUser: Boolean) {
        val basket = viewModel.basket.value
        menu?.findItem(R.id.basket_remove_button)?.isVisible = isCurrentUser && basket != null
        menu?.findItem(R.id.basket_edit_button)?.isVisible = isCurrentUser && basket != null

        if (!isCurrentUser && basket != null && basket.contactTypes.isNotEmpty()) {
            if (basket.contactTypes.contains(Basket.CONTACT_TYPE_MESSAGE)) {
                binding.basketRequestCountLabel.visibility = VISIBLE
                binding.basketRequestCountLabel.text = resources.getQuantityString(
                    R.plurals.basket_request_number,
                    basket.requestCount,
                    basket.requestCount
                )

                val currentUserHasRequested = basket.requests.isNotEmpty()
                if (currentUserHasRequested) {
                    setVisible(binding.basketMessageButton, binding.basketWithdrawRequestButton)
                    binding.basketRequestButton.setGone()

                    binding.basketMessageButton.setOnClickListener {
                        ConversationActivity.start(this, basket.creator.id)
                    }

                    binding.basketWithdrawRequestButton.setOnClickListener {
                        Utils.showQuestionDialog(this, getString(R.string.basket_withdraw_question)) { result ->
                            if (result) {
                                viewModel.withdrawRequest()
                            }
                        }
                    }
                } else {
                    binding.basketRequestButton.setVisible()
                    setGone(binding.basketMessageButton, binding.basketWithdrawRequestButton)

                    binding.basketRequestButton.setOnClickListener {
                        lateinit var dialog: AlertDialog
                        val builder = AlertDialog.Builder(this)
                        builder.setTitle(R.string.request_button)
                        builder.setView(R.layout.dialog_request_basket)
                        builder.setPositiveButton(R.string.basket_request_dialog_label) { _, _ ->
                            viewModel.requestBasket(
                                dialog.findViewById<EditText>(R.id.basket_request_message_input)?.text.toString()
                            )
                        }
                        builder.setNegativeButton(android.R.string.cancel, null)
                        dialog = builder.show()
                    }
                }
            } else {
                setGone(
                    binding.basketMessageButton,
                    binding.basketRequestCountLabel,
                    binding.basketRequestButton,
                    binding.basketWithdrawRequestButton
                )
            }

            if (basket.contactTypes.contains(Basket.CONTACT_TYPE_PHONE)) {
                binding.basketPhone.setVisible()
                with(basket) {
                    if (phone.isNotEmpty()) {
                        binding.basketPhoneInfo.text = phone
                        setVisible(binding.basketPhoneLabel, binding.basketPhoneInfo)
                    }
                    if (mobile.isNotEmpty()) {
                        binding.basketMobileInfo.text = mobile
                        setVisible(binding.basketMobileLabel, binding.basketMobileInfo)
                    }
                }
            } else {
                binding.basketPhone.setGone()
            }
        } else {
            setGone(
                binding.basketMessageButton,
                binding.basketRequestCountLabel,
                binding.basketRequestButton,
                binding.basketWithdrawRequestButton
            )
        }
    }

    private fun formatDate(date: Date): String {
        val now = Calendar.getInstance()
        now.time = Date()

        val cal: Calendar = Calendar.getInstance()
        cal.time = date

        val format = if (cal[Calendar.YEAR] == now[Calendar.YEAR]) dateFormatShort else dateFormat
        return format.format(date)
    }
}
