package de.foodsharing.ui.map

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import de.foodsharing.R
import de.foodsharing.databinding.ActivityMapBinding
import de.foodsharing.di.Injectable
import de.foodsharing.ui.base.BaseActivity

class MapActivity : BaseActivity(), Injectable {

    private lateinit var binding: ActivityMapBinding

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, MapActivity::class.java)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMapBinding.inflate(layoutInflater)
        rootLayoutID = binding.root.id
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }

        else -> super.onOptionsItemSelected(item)
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right)
    }
}
