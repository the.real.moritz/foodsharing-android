package de.foodsharing.ui.baskets

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import de.foodsharing.R
import de.foodsharing.databinding.FragmentNearbyBasketsBinding
import de.foodsharing.services.PreferenceManager
import de.foodsharing.ui.base.BaseFragment
import de.foodsharing.ui.base.EventObserver
import de.foodsharing.ui.base.LocationFilterComponent
import de.foodsharing.ui.basket.BasketActivity
import de.foodsharing.utils.NonScrollingLinearLayoutManager
import de.foodsharing.utils.UserLocation
import javax.inject.Inject

class NearbyBasketsFragment : BaseFragment() {

    private var _binding: FragmentNearbyBasketsBinding? = null

    private val binding get() = _binding!!

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var userLocation: UserLocation

    @Inject
    lateinit var preferenceManager: PreferenceManager

    private val viewModel: NearbyBasketsViewModel by viewModels { viewModelFactory }

    private lateinit var adapter: NearbyBasketListAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentNearbyBasketsBinding.inflate(inflater, container, false)

        binding.recyclerView.layoutManager = NonScrollingLinearLayoutManager(activity)

        adapter = NearbyBasketListAdapter({ basket ->
            BasketActivity.start(requireContext(), basket.id)
            activity?.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
        }, requireContext())
        binding.recyclerView.adapter = adapter

        bindViewModel()

        userLocation.currentCoordinates.observe(viewLifecycleOwner) {
            it.let {
                val prevCoordinate = userLocation.previousCoordinate
                if (it.lat != 0.0 && it.lon != 0.0 && prevCoordinate.lat != it.lat && prevCoordinate.lon != it.lon) {
                    viewModel.reload()
                }
            }
        }

        binding.nearbyBasketsSettingsButton.setOnClickListener {
            showSettingsDialog()
        }

        return binding.root
    }

    private fun bindViewModel() {
        viewModel.setDistance(preferenceManager.nearbyBasketsDistance)
        viewModel.isLoading.observe(viewLifecycleOwner) {
            if (it) {
                binding.noNearbyBasketsLabel.visibility = View.GONE
                binding.progressBar.visibility = View.VISIBLE
            } else {
                binding.progressBar.visibility = View.GONE
            }
        }

        viewModel.showError.observe(
            viewLifecycleOwner,
            EventObserver {
                showMessage(getString(it))
            }
        )

        viewModel.baskets.observe(viewLifecycleOwner) {
            if (it.isEmpty()) {
                binding.noNearbyBasketsLabel.visibility = View.VISIBLE
                binding.recyclerView.visibility = View.GONE
                adapter.setBaskets(it)
            } else {
                binding.recyclerView.visibility = View.VISIBLE
                binding.noNearbyBasketsLabel.visibility = View.GONE
                adapter.setBaskets(it)
            }
        }
    }

    private fun showSettingsDialog() {
        LocationFilterComponent.showDialog(
            requireActivity(),
            preferenceManager.nearbyBasketsLocationType,
            preferenceManager.nearbyBasketsDistance
        ) { locationType, distance ->
            preferenceManager.nearbyBasketsLocationType = locationType
            preferenceManager.nearbyBasketsDistance = distance
            viewModel.setDistance(distance)
            reload()
        }
    }

    fun reload() {
        viewModel.reload()
    }
}
