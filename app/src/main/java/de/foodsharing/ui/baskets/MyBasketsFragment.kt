package de.foodsharing.ui.baskets

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import de.foodsharing.R
import de.foodsharing.databinding.FragmentMyBasketsBinding
import de.foodsharing.services.PreferenceManager
import de.foodsharing.ui.base.BaseFragment
import de.foodsharing.ui.base.EventObserver
import de.foodsharing.ui.basket.BasketActivity
import de.foodsharing.utils.NonScrollingLinearLayoutManager
import javax.inject.Inject

class MyBasketsFragment : BaseFragment() {

    private var _binding: FragmentMyBasketsBinding? = null

    private val binding get() = _binding!!

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var preferenceManager: PreferenceManager

    private val viewModel: MyBasketsViewModel by viewModels { viewModelFactory }

    private lateinit var adapter: MyBasketListAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMyBasketsBinding.inflate(inflater, container, false)
        binding.recyclerView.layoutManager = NonScrollingLinearLayoutManager(activity)

        adapter = MyBasketListAdapter({ basket ->
            BasketActivity.start(requireContext(), basket.id)
            activity?.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
        }, requireContext())
        binding.recyclerView.adapter = adapter

        bindViewModel()

        return binding.root
    }

    private fun bindViewModel() {
        viewModel.isLoading.observe(viewLifecycleOwner) {
            if (it) {
                binding.noPublishedBasketsLabel.visibility = View.GONE
                binding.progressBar.visibility = View.VISIBLE
            } else {
                binding.progressBar.visibility = View.GONE
            }
        }

        viewModel.showError.observe(
            viewLifecycleOwner,
            EventObserver {
                showMessage(getString(it))
            }
        )

        viewModel.baskets.observe(viewLifecycleOwner) {
            if (it.isEmpty()) {
                binding.recyclerView.visibility = View.GONE
                binding.noPublishedBasketsLabel.visibility = View.VISIBLE
            } else {
                binding.recyclerView.visibility = View.VISIBLE
                binding.noPublishedBasketsLabel.visibility = View.GONE

                adapter.setBaskets(it)
            }
        }
    }

    fun reload() {
        viewModel.reload()
    }
}
