package de.foodsharing.ui.baskets

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import de.foodsharing.R
import de.foodsharing.databinding.ItemBasketBinding
import de.foodsharing.model.Basket
import de.foodsharing.utils.BASE_URL

class MyBasketListAdapter(
    private val onClickListener: (Basket) -> Unit,
    val context: Context
) : RecyclerView.Adapter<MyBasketListAdapter.BasketHolder>() {
    private var baskets: List<Basket> = emptyList()

    fun setBaskets(newBaskets: List<Basket>) {
        val diffResult = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
            override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                return baskets[oldItemPosition].id == newBaskets[newItemPosition].id
            }

            override fun getOldListSize(): Int {
                return baskets.size
            }

            override fun getNewListSize(): Int {
                return newBaskets.size
            }

            override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                return baskets[oldItemPosition] == newBaskets[newItemPosition]
            }
        })

        baskets = newBaskets
        diffResult.dispatchUpdatesTo(this)
    }

    override fun getItemCount(): Int = baskets.size

    override fun onBindViewHolder(holder: BasketHolder, position: Int) {
        if (position in 0 until itemCount) {
            holder.bind(baskets[position])
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewInt: Int): BasketHolder {
        val binding = ItemBasketBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return BasketHolder(
            binding,
            onClickListener,
            context
        )
    }

    class BasketHolder(
        val binding: ItemBasketBinding,
        private val onClickListener: (Basket) -> Unit,
        val context: Context
    ) : RecyclerView.ViewHolder(binding.root), View.OnClickListener {

        private var basket: Basket? = null

        init {
            binding.root.setOnClickListener(this)
        }

        override fun onClick(v: View) {
            basket?.let {
                onClickListener(it)
            }
        }

        fun bind(basket: Basket) {
            this.basket = basket

            // view.item_creation_date.text = formatter.format(basket.createdAt)
            binding.itemDescription.text = basket.description

            if (!basket.picture.isNullOrEmpty()) {
                binding.itemPicture.visibility = View.VISIBLE
                val pictureUrl = "$BASE_URL/images/basket/${basket.picture}"
                Glide.with(context)
                    .load(pictureUrl)
                    .apply(RequestOptions().transform(CenterCrop(), RoundedCorners(8)))
                    .error(R.drawable.basket_default_picture)
                    .into(binding.itemPicture)
            } else {
                binding.itemPicture.visibility = View.GONE
            }

            binding.root.setBackgroundColor(
                ContextCompat.getColor(context, R.color.white)
            )
            binding.itemTitleText.text = context.getString(R.string.basket_title_mine)
            binding.itemDistance.visibility = View.GONE
        }
    }
}
