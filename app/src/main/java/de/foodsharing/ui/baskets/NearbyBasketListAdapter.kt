package de.foodsharing.ui.baskets

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import de.foodsharing.R
import de.foodsharing.databinding.ItemBasketBinding
import de.foodsharing.model.Basket
import de.foodsharing.utils.BASE_URL
import de.foodsharing.utils.Utils
import de.foodsharing.utils.getDisplayName
import java.util.Locale

// TODO: combine with MyBasketListAdapter
class NearbyBasketListAdapter(
    private val onClickListener: (Basket) -> Unit,
    private val context: Context
) : RecyclerView.Adapter<NearbyBasketListAdapter.BasketHolder>() {

    private var baskets: List<BasketItemModel> = emptyList()

    fun setBaskets(newBaskets: List<BasketItemModel>) {
        val diffResult = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
            override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                return baskets[oldItemPosition].basket.id == newBaskets[newItemPosition].basket.id
            }

            override fun getOldListSize(): Int {
                return baskets.size
            }

            override fun getNewListSize(): Int {
                return newBaskets.size
            }

            override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                return baskets[oldItemPosition].basket == newBaskets[newItemPosition].basket
            }
        })

        baskets = newBaskets
        diffResult.dispatchUpdatesTo(this)
    }

    override fun getItemCount(): Int = baskets.size

    override fun onBindViewHolder(holder: BasketHolder, position: Int) {
        if (position in 0 until itemCount) {
            holder.bind(baskets[position])
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewInt: Int): BasketHolder {
        val binding = ItemBasketBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return BasketHolder(binding, onClickListener, context)
    }

    class BasketHolder(
        private val binding: ItemBasketBinding,
        private val onClickListener: (Basket) -> Unit,
        private val context: Context
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: BasketItemModel) {
            val basket = item.basket

            binding.root.setOnClickListener {
                onClickListener(basket)
            }

            // view.item_creation_date.text = formatter.format(basket.createdAt)
            binding.itemDescription.text = basket.description.trim()

            if (!basket.picture.isNullOrEmpty()) {
                binding.itemPicture.visibility = View.VISIBLE
                val pictureUrl = "$BASE_URL/images/basket/${basket.picture}"
                Glide.with(context)
                    .load(pictureUrl)
                    .apply(RequestOptions().transform(CenterCrop(), RoundedCorners(8)))
                    .error(R.drawable.basket_default_picture)
                    .into(binding.itemPicture)
            } else {
                binding.itemPicture.visibility = View.GONE
            }

            binding.itemTitleText.text =
                context.getString(R.string.basket_title, basket.creator.getDisplayName(context))
                    .uppercase(Locale.getDefault())

            if (item.distance != null) {
                binding.itemDistance.visibility = View.VISIBLE
                binding.itemDistance.text = Utils.formatDistance(item.distance)
            } else {
                binding.itemDistance.visibility = View.GONE
            }
        }
    }
}
