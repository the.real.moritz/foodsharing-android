package de.foodsharing.ui.posts

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import de.foodsharing.databinding.ItemPostPictureBinding
import de.foodsharing.model.PostPicture
import de.foodsharing.ui.picture.PictureActivity

class PostPictureListAdapter(val context: Context) :
    RecyclerView.Adapter<PostPictureListAdapter.PostPictureHolder>() {
    private var pictures = emptyList<PostPicture>()

    override fun getItemCount() = pictures.size

    override fun onBindViewHolder(holder: PostPictureHolder, position: Int) {
        if (position in 0 until itemCount) {
            holder.bind(pictures[position])
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewInt: Int): PostPictureHolder {
        val binding = ItemPostPictureBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return PostPictureHolder(binding, context)
    }

    fun setPictures(pictures: List<PostPicture>) {
        this.pictures = pictures
        notifyDataSetChanged()
    }

    class PostPictureHolder(val binding: ItemPostPictureBinding, val context: Context) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(picture: PostPicture) {
            binding.postPicture.let {
                Glide.with(context)
                    .load(picture.mediumSizeUrl())
                    .into(it)

                it.setOnClickListener {
                    PictureActivity.start(context, picture.fullSizeUrl())
                }
            }
        }
    }
}
