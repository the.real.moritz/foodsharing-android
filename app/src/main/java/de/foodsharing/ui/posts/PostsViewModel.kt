package de.foodsharing.ui.posts

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import de.foodsharing.R
import de.foodsharing.api.PostsAPI
import de.foodsharing.model.Post
import de.foodsharing.services.AuthService
import de.foodsharing.ui.base.BaseViewModel
import de.foodsharing.ui.base.Event
import java.io.IOException
import javax.inject.Inject

class PostsViewModel @Inject constructor(
    private val postsAPI: PostsAPI,
    auth: AuthService
) : BaseViewModel() {

    fun posts(): LiveData<List<Post>> = posts
    fun mayPost(): LiveData<Boolean> = mayPost
    fun mayDelete(): LiveData<Boolean> = mayDelete
    fun isLoading(): LiveData<Boolean> = isLoading
    fun error(): LiveData<Event<Int>> = error
    val currentUserId = auth.currentUser?.id

    private val posts = MutableLiveData<List<Post>>().apply { value = emptyList() }
    private val mayPost = MutableLiveData<Boolean>().apply { value = false }
    private val mayDelete = MutableLiveData<Boolean>().apply { value = false }
    private val isLoading = MutableLiveData<Boolean>().apply { value = false }
    private val error = MutableLiveData<Event<Int>>()

    fun fetchPosts(target: PostsAPI.Target, id: Int) {
        request(
            postsAPI.getPosts(target.value, id).doOnNext {
                isLoading.postValue(true)
            },
            { postsResponse ->
                isLoading.value = false
                posts.value = postsResponse.results
                mayPost.value = postsResponse.mayPost
                mayDelete.value = postsResponse.mayDelete
            },
            { handleError(it) }
        )
    }

    fun sendPost(target: PostsAPI.Target, id: Int, message: String, onSuccess: () -> Unit) {
        if (message.isNotBlank()) {
            request(
                postsAPI.sendPost(target.value, id, message).doOnNext {
                    isLoading.postValue(true)
                },
                { response ->
                    isLoading.value = false
                    posts.value = listOf(response.post) + posts.value.orEmpty()
                    onSuccess()
                },
                { handleError(it) }
            )
        }
    }

    fun deletePost(target: PostsAPI.Target, id: Int, postId: Int) {
        request(
            postsAPI.deletePost(target.value, id, postId).doOnNext {
                isLoading.postValue(true)
            },
            {
                isLoading.value = false
                posts.value = posts.value.orEmpty().filter { it.id != postId }
            },
            { handleError(it) }
        )
    }

    private fun handleError(throwable: Throwable) {
        isLoading.value = false
        throwable.printStackTrace()
        when (throwable) {
            is IOException -> {
                // Network Error
                error.postValue(Event(R.string.error_no_connection))
            }

            else -> {
                // HttpException or unknown exception
                error.postValue(Event(R.string.error_unknown))
            }
        }
    }
}
