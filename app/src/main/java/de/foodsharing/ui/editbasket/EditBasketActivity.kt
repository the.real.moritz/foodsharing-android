package de.foodsharing.ui.editbasket

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.core.os.bundleOf
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import de.foodsharing.R
import de.foodsharing.databinding.ActivityEditBasketBinding
import de.foodsharing.di.Injectable
import de.foodsharing.model.Basket
import de.foodsharing.model.Coordinate
import de.foodsharing.ui.base.AuthRequiredBaseActivity
import de.foodsharing.ui.base.EventObserver
import de.foodsharing.ui.basket.PickLocationActivity
import de.foodsharing.ui.editbasket.EditBasketActivity.Companion.EXTRA_BASKET
import de.foodsharing.ui.picture.PictureFragment
import de.foodsharing.utils.BASE_URL
import de.foodsharing.utils.DETAIL_MAP_ZOOM
import de.foodsharing.utils.LOG_TAG
import de.foodsharing.utils.OsmdroidUtils
import de.foodsharing.utils.Utils.getBasketMarkerIconBitmap
import javax.inject.Inject

/**
 * Activity that handles editing baskets. It requires a valid basket as an extra with
 * [EXTRA_BASKET].
 */
class EditBasketActivity : AuthRequiredBaseActivity(), Injectable {

    companion object {
        private const val EXTRA_BASKET = "basket"

        private const val STATE_DESCRIPTION = "description"
        private const val STATE_COORDINATE = "coordinate"

        fun start(context: Context, basket: Basket) {
            val extras = bundleOf(EXTRA_BASKET to basket)
            val intent = Intent(context, EditBasketActivity::class.java).putExtras(extras)
            context.startActivity(intent)
        }
    }

    private lateinit var binding: ActivityEditBasketBinding

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: EditBasketViewModel by viewModels { viewModelFactory }
    private var mapSnapshotDetachAction: (() -> Unit)? = null
    private lateinit var location: Coordinate
    private var pictureWasChanged = false
    private var pickLocationLauncher = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()
    ) { result: ActivityResult ->
        if (result.resultCode == RESULT_OK && result.data != null) {
            result.data!!.getParcelableExtra<Coordinate?>(PickLocationActivity.EXTRA_COORDINATE)
                ?.let { updateBasketMap(it) }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEditBasketBinding.inflate(layoutInflater)
        rootLayoutID = binding.root.id
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = getString(R.string.basket_edit_title)

        bindViewModel()

        val basket = intent.getSerializableExtra(EXTRA_BASKET) as Basket
        viewModel.basket.value = basket

        val pictureFragment = findPictureFragment()
        pictureFragment.setCanTakePhoto(true)
        if (!basket.picture.isNullOrEmpty()) {
            basket.picture?.let {
                val pictureUri = "$BASE_URL/images/basket/${basket.picture}"
                pictureFragment.pictureUri = pictureUri
            }
        }
        pictureFragment.pictureWasChanged.observe(
            this,
            EventObserver {
                pictureWasChanged = true
            }
        )

        updateBasketMap(basket.toCoordinate())

        binding.basketUpdateButton.setOnClickListener { updateBasket() }

        if (savedInstanceState == null) {
            binding.basketDescriptionInput.setText(basket.description)
        } else if (savedInstanceState.containsKey(STATE_DESCRIPTION)) {
            binding.basketDescriptionInput.setText(savedInstanceState.getString(STATE_DESCRIPTION))
            updateBasketMap(savedInstanceState.getParcelable(STATE_COORDINATE)!!)
        }
    }

    private fun bindViewModel() {
        viewModel.isLoading.observe(this) {
            binding.progressBar.visibility = if (it) VISIBLE else View.INVISIBLE
        }

        viewModel.showError.observe(
            this,
            EventObserver {
                binding.progressBar.visibility = GONE
                showMessage(getString(it))
                binding.basketUpdateButton.isEnabled = true
            }
        )

        viewModel.basketUpdated.observe(
            this,
            EventObserver {
                finish()
            }
        )
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }

        else -> super.onOptionsItemSelected(item)
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right)
    }

    override fun onDestroy() {
        mapSnapshotDetachAction?.invoke()
        super.onDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString(STATE_DESCRIPTION, binding.basketDescriptionInput.text.toString())
        outState.putParcelable(STATE_COORDINATE, location)

        super.onSaveInstanceState(outState)
    }

    /* override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            REQUEST_PICK_LOCATION -> {
                if (resultCode == RESULT_OK && data != null) {
                    data.getParcelableExtra<Coordinate?>(PickLocationActivity.EXTRA_COORDINATE)
                        ?.let { updateBasketMap(it) }
                }
            }
        }
    } */

    /**
     * Checks the input and updates the basket.
     */
    private fun updateBasket() {
        binding.basketUpdateButton.isEnabled = false

        val description = binding.basketDescriptionInput.text.toString().trim()
        if (description.isEmpty()) {
            showMessage(getString(R.string.basket_error_description), Snackbar.LENGTH_LONG)
            binding.basketUpdateButton.isEnabled = true
        } else {
            binding.progressBar.visibility = VISIBLE
            val pictureFile = findPictureFragment().file
            Log.e(LOG_TAG, "pictureChanged: $pictureWasChanged, file: ${pictureFile?.toString()}")
            viewModel.update(description, location, pictureFile, pictureWasChanged)
        }
    }

    private fun findPictureFragment() =
        supportFragmentManager.findFragmentById(R.id.basket_picture_view) as PictureFragment

    private fun updateBasketMap(location: Coordinate) {
        this.location = location

        mapSnapshotDetachAction = OsmdroidUtils.loadMapTileToImageView(
            binding.basketLocationView,
            location,
            DETAIL_MAP_ZOOM,
            getBasketMarkerIconBitmap(applicationContext),
            preferences.allowHighResolutionMap
        )

        binding.basketLocationView.setOnClickListener {
            val intent = Intent(this, PickLocationActivity::class.java)
            intent.putExtra(PickLocationActivity.EXTRA_COORDINATE, location)
            intent.putExtra(PickLocationActivity.EXTRA_MARKER_ID, R.string.marker_basket_id)
            intent.putExtra(PickLocationActivity.EXTRA_ZOOM, DETAIL_MAP_ZOOM)

            pickLocationLauncher.launch(intent)
        }
    }
}
